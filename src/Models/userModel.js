var{mongoose, conn} = require("../Modules/connection");
const { Schema} = require('mongoose');
let  userSchema  = mongoose.Schema(
    {
        access_token : {
            type : String,
        },  
        name :String ,
        password: {
            type : String,
            default : "N/A"
        },
        email : {
            type: String,
        },
        mobileNumber : {
            type : String,
        },
        socail_id : String ,
        socail_type : {
            type : String ,
            enum :['google','facebook'],
        } , 
        profileImage: {
            type: String,
            default: null
        },
        is_signup_complete: {
            type : String ,
            default : '0'
        } ,
        created_on : {
            type : Number,
            default :Date.now()
        },
     },
);
exports.UserModel = conn.model('User', userSchema); 
